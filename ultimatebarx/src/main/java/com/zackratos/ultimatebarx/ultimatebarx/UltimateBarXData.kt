package com.zackratos.ultimatebarx.ultimatebarx

import android.content.Context
import androidx.fragment.app.Fragment
import com.zackratos.ultimatebarx.ultimatebarx.extension.getRom
import com.zackratos.ultimatebarx.ultimatebarx.rom.Rom
import java.lang.reflect.Field

/**
 * @Author   : zhangwenchao
 * @Date     : 2020/6/26  12:32 PM
 * @email    : 869649338@qq.com
 * @Describe : 管理类，主要用于存储 Activity 和 Fragment 当前的状态
 */
internal class UltimateBarXData private constructor(){
    companion object {
        val instance: UltimateBarXData
            get() = Holder.INSTANCE
    }

    private object Holder {
        val INSTANCE = UltimateBarXData()
    }

    internal val rom: Rom by lazy { getRom() }

    internal lateinit var ultimateContext: Context

    internal val fragmentViewFiled: Field by lazy { Fragment::class.java.getDeclaredField("mView").apply { isAccessible = true } }

}