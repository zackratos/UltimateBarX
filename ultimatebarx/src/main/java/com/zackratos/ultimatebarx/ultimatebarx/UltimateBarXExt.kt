package com.zackratos.ultimatebarx.ultimatebarx

import android.content.Context
import android.os.Build
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.zackratos.ultimatebarx.ultimatebarx.rom.Rom
import java.lang.reflect.Field

/**
 * @Author: Zackratos
 * @Email: 869649338@qq.com
 * @Time: 2023/5/7 20:33
 * @Description:
 */

internal val ViewModelStoreOwner.ultimateBarXVM: UltimateBarXViewModel
    get() = ViewModelProvider(this)[UltimateBarXViewModel::class.java]

internal val ultimateContext: Context
    get() = UltimateBarXData.instance.ultimateContext

internal val rom: Rom
    get() = UltimateBarXData.instance.rom

internal val fragmentViewFiled: Field
    get() = UltimateBarXData.instance.fragmentViewFiled

internal val needApply: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT