package com.zackratos.ultimatebarx.ultimatebarx

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.zackratos.ultimatebarx.ultimatebarx.bean.BarConfig
import com.zackratos.ultimatebarx.ultimatebarx.core.*
import com.zackratos.ultimatebarx.ultimatebarx.core.addObserver
import com.zackratos.ultimatebarx.ultimatebarx.core.defaultNavigationBar
import com.zackratos.ultimatebarx.ultimatebarx.core.ultimateBarXInitialization
import com.zackratos.ultimatebarx.ultimatebarx.core.updateStatusBar
import com.zackratos.ultimatebarx.ultimatebarx.extension.setStatusBarSystemUiFlagWithLight
import com.zackratos.ultimatebarx.ultimatebarx.extension.setSystemUiFlagWithLight

/**
 * @Author   : zackratos
 * @Date     : 2021/8/26 10:26 上午
 * @email    : zhangwenchao@soulapp.cn
 * @Describe :
 */
@RequiresApi(Build.VERSION_CODES.KITKAT)
internal fun FragmentActivity.applyStatusBar(config: BarConfig) {
    ultimateBarXInitialization()
    val navLight = ultimateBarXVM.navigationBarConfig.light
    setSystemUiFlagWithLight(config.light, navLight)
    updateStatusBar(config)
    defaultNavigationBar()
}

@RequiresApi(Build.VERSION_CODES.KITKAT)
internal fun FragmentActivity.applyNavigationBar(config: BarConfig) {
    ultimateBarXInitialization()
    val staLight = ultimateBarXVM.statusBarConfig.light
    setSystemUiFlagWithLight(staLight, config.light)
    updateNavigationBar(config)
    defaultStatusBar()
}

@RequiresApi(Build.VERSION_CODES.KITKAT)
internal fun Fragment.applyStatusBar(config: BarConfig) {
    requireActivity().ultimateBarXInitialization()
    ultimateBarXInitialization()
    val navLight =ultimateBarXVM.navigationBarConfig.light
    requireActivity().setSystemUiFlagWithLight(config.light, navLight)
    updateStatusBar(config)
    requireActivity().defaultNavigationBar()
    addObserver()
}

@RequiresApi(Build.VERSION_CODES.KITKAT)
internal fun Fragment.applyNavigationBar(config: BarConfig) {
    requireActivity().ultimateBarXInitialization()
    ultimateBarXInitialization()
    val staLight = ultimateBarXVM.statusBarConfig.light
    requireActivity().setSystemUiFlagWithLight(staLight, config.light)
    updateNavigationBar(config)
    requireActivity().defaultStatusBar()
    addObserver()
}

@RequiresApi(Build.VERSION_CODES.KITKAT)
internal fun FragmentActivity.applyStatusBarOnly(config: BarConfig) {
    statusBarOnlyInitialization()
    setStatusBarSystemUiFlagWithLight(config.light)
    updateStatusBar(config)
}

@RequiresApi(Build.VERSION_CODES.KITKAT)
internal fun Fragment.applyStatusBarOnly(config: BarConfig) {
    requireActivity().statusBarOnlyInitialization()
    statusBarOnlyInitialization()
    requireActivity().setStatusBarSystemUiFlagWithLight(config.light)
    updateStatusBar(config)
    addObserver(true)
}