package com.zackratos.ultimatebarx.ultimatebarx

import androidx.lifecycle.ViewModel
import com.zackratos.ultimatebarx.ultimatebarx.bean.BarConfig

/**
 * @Author: Zackratos
 * @Email: 869649338@qq.com
 * @Time: 2023/5/7 20:27
 * @Description:
 */
internal class UltimateBarXViewModel: ViewModel() {
    // 保存是否已经初始化
    var initializationed: Boolean = false
    var statusBarDefaulted: Boolean = false
    var navigationBarDefaulted: Boolean = false
    var statusBarConfig: BarConfig = BarConfig.newInstance()
    var navigationBarConfig: BarConfig = BarConfig.newInstance()

    var addedObserver: Boolean = false


}